module.exports = {
    socket : {
        ioPort: 5681,
        backConnectPort : 19901
    },
    sphinx : {
        host: '192.168.1.7',
        port: 9312
    },
    redis : {
        host: '192.168.1.7',
        port: 6380,
        addedSettings: {'retry_max_delay': 5000}
    },
    node : {
        version: '0.11',
        environment: 'dev'
    }
};
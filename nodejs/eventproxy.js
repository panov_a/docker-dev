#!/usr/bin/node
if (typeof(process.argv[2]) == 'undefined') {
	console.log("Usage: ".process.argv[0]+" "+process.argv[1]+" test/prod");
}

var settings    = require('./settings-'+process.argv[2]+'.js');

var crypto = require('crypto');
var redis = require('redis');
var PHPUnserialize = require('php-unserialize');

var preparedQueryToUpdateUsers = "UPDATE rt_orders SET is_customer_online = ? WHERE customer_id = ?";

var app = require('express')();
var http = require('http');
var server = require('http').Server(app);
var io = require('socket.io')(server, {
    pingInterval: 10000,
    pingTimeout: 10000
});

var mysql = require('mysql');

var sockets = {};
var socketToUser = {};
var userToSocket = {};
var online = 0;
var eventCount = 0;
var lastEventCount = 0;
var sphinxUpdateCount = 0;
var lastSphinxUpdateCount = 0;

var net = require('net');

var startTime = Date.now();

var performNetPushEvent = function(request) {
	console.log(request);
};

var backConnect = net.createServer(function(socket){
	socket.setEncoding('utf8');
	socket.on('data', function(data) {
		var request = {'method': 'mock', 'params' : {}};
		try {
			request = JSON.parse(data);
		} catch (e) {
			// Bad request
		}

		var response = false;
		if (typeof(request['method']) == 'undefined') {
			request = {'method': 'mock', 'params' : {}};
		}

		switch (request['method']) {
			case 'mock':
				break;
			case 'userOnline':
				if (typeof(request.params['userId']) !== 'undefined') {
					response = (typeof(userToSocket[request.params['userId']]) !== 'undefined');
				}
				break;
			case 'usersOnline':
				if (typeof(request.params['usersIds']) !== 'undefined') {
					response = {};
					for (var userId in request.params['usersIds']) {
						response[userId] = (typeof(userToSocket[request.params['userId']]) !== 'undefined');
					}
				}
				break;
			case 'usersOnlineArray':
				response = [];
				for (var user in userToSocket) {
					response.push(+user);
				}
				break;
			case 'pushEvent':
				if (typeof(request.params['userId']) !== 'undefined' && typeof(request.params['event']) !== 'undefined') {
					var userId = request.params['userId'];
					var event = request.params['event'];
					eventCount++;
					console.log('Sending #'+userId+' data: '+JSON.stringify(event));
					if (typeof(userToSocket[userId]) !== 'undefined') {
						if (typeof(event['type']) !== 'undefined') {
							for (var key in userToSocket[userId]) {
								sockets[userToSocket[userId][key]].emit(event['type'], (typeof(event['data']) == 'undefined') ? null : event['data']);
							}
							response = true;
						}
					}}
				break;
			 case 'multicastEvent':
				console.log("Get multicast event");
				console.log(request.params);
				console.log(userToSocket);
                                var response = [];
                                var event = request.params['event'];
				request.params['userIds'].forEach(function(userId){
//                                for (var key, userId in request.params['userIds'].forEacy) {
					console.log(userId);
					console.log(typeof(userToSocket[userId]));
                                        if (typeof(userToSocket[userId]) !== 'undefined') {
                                            eventCount++;
                                            console.log('Sending #'+userId+' data: '+JSON.stringify(event));
                                                if (typeof(event['type']) !== 'undefined') {
                                                        for (var key in userToSocket[userId]) {
                                                                sockets[userToSocket[userId][key]].emit(event['type'], (typeof(event['data']) == 'undefined') ? null : event['data']);
                                                        }
                                                        response.push(userId);
                                                }
                                        }
                                });
                                break;
		}
		socket.write(JSON.stringify(response)+"\n");
	});

	socket.on('error', function(err) {});
}).listen(settings.socket.backConnectPort);

//--------------------------- Endof backport

var redisClient = redis.createClient(settings.redis.port, settings.redis.host, settings.redis.addedSettings);
redisClient.on("error", function (err) {
	console.log("Error" + err);
});

server.listen(settings.socket.ioPort);

var mySqlClient = initializeConnection(settings.sphinx);

function initializeConnection(config) {
	console.log(config);
	var connection = mysql.createConnection(config);

	connection.on("error", function (error) {
		if (error instanceof Error) {
			if (error.code === "PROTOCOL_CONNECTION_LOST") {
				console.error(error.stack);
				console.log("Lost connection. Reconnecting...");

				mySqlClient = initializeConnection(connection.config);
			} else if (error.fatal) {
				throw error;
			}
		}
	});

	connection.connect();
	return connection;
}


//--------------------------------------------------------------------------------------------------------------------------------
// Stats, performance and GC
var perfTicks = [];
var perfStat = {"avg": 10, "max": 10, "90p": 10};

app.get('/stats/', function (req, res) {
	var memUsage = process.memoryUsage();
	res.set("Content-type", "text/plain");
	var response = 'rss='+memUsage['rss']+"\n";
	response += 'heapTotal='+memUsage['heapTotal']+"\n";
	response += 'heapUsed='+memUsage['heapUsed']+"\n";
	response += 'connections='+online+"\n";
	response += 'perf_avg='+perfStat["avg"]+"\n";
	response += 'perf_max='+perfStat["max"]+"\n";
	response += 'pref_90p='+perfStat["90p"]+"\n";
	response += 'uptime='+((Date.now() - startTime)/1000).toFixed()+"\n";
	response += 'events='+lastEventCount+"\n";
	response += 'sphinxUpdateCount='+lastSphinxUpdateCount+"\n";
	response += 'nodeVersion='+settings.node.version+' ('+settings.node.environment+")\n";
	res.send(response);
});

app.get('/connections/', function(req,res) {
	var response = '';
	res.set("Content-type", "text/plain");
	for (var socketId in sockets) {
		response += "socketId="+socketId + "; userId=" + sockets[socketId].handshake.userId + "; remote_addr=" + sockets[socketId].handshake.headers['x-real-ip'] + "\n";
	}
	res.send(response);
});

app.get('/usersOnlineArray/', function(req,res) {
	var response = [];
	res.set("Content-type", "text/plain");
	for (var user in userToSocket) {
		response.push(+user);
	}

	res.send(JSON.stringify(response));
});

setInterval(function() {
	perfTicks.push(Date.now());
}, 10);

setInterval(function() {
	var prevTick = false;
	var countedTicks = [];
	var sum = 0;
	var max = -1;
	for (var ticknum in perfTicks) {
		if (ticknum == 0) continue;
		var dif = perfTicks[ticknum] - perfTicks[ticknum-1];
		countedTicks.push(dif);
		if (dif > max) {
			max = dif;
		}
		sum += dif;
	}
	perfStat['avg'] = (sum / countedTicks.length).toFixed(2);
	perfStat['max'] = max.toFixed(2);

	countedTicks.sort();
	var pCount = Math.ceil(0.9 * countedTicks.length);
	var pSum = 0;
	for (var i=0;i<pCount;i++) {
		pSum += countedTicks[i];
	}
	perfStat['90p'] = (pSum / pCount).toFixed(2);
	perfTicks.length=0;
	lastEventCount = eventCount;
	eventCount = 0;

	lastSphinxUpdateCount = sphinxUpdateCount;
	sphinxUpdateCount = 0;
}, 60000);

// Perform GC collecting
setInterval(function() {
	gc();
}, 300000);
//--------------------------------------------------------------------------------------------------------------------------------


io.sockets.on('connection', function (client) {
	sockets[client['id']] = client;
	online++;

	socketToUser[client['id']] = client.handshake.userId;
	if (typeof(userToSocket[client.handshake.userId]) == 'undefined') {
		userToSocket[client.handshake.userId] = [];
		updateUserOrders(client.handshake.userId, true);
	}

	userToSocket[client.handshake.userId].push(client['id']);


	client.on('disconnect', function() {
		if (typeof(sockets[client['id']]) !== 'undefined') {
			delete sockets[client['id']];
			var localUser = socketToUser[client['id']];

			delete socketToUser[client['id']];
			var unlinkKey = 0;
			for (var key in userToSocket[localUser]) {
				if (userToSocket[localUser][key] == client['id']) {
					unlinkKey = key;
				}
			}
			userToSocket[localUser].splice(unlinkKey, 1);
			if (!userToSocket[localUser].length) {
				delete userToSocket[localUser];
			}
			setTimeout(function() {
				if (typeof userToSocket[localUser] === 'undefined') {
					updateUserOrders(client.handshake.userId, false);
				}
			}, 3000); // Timeout - because of duel on clients

			online--;
		}

	});

});


io.use(function(socket,next) {
	var hsData = socket.request;

	if (typeof(hsData._query['socketId']) == 'undefined') {
		next(new Error('mandatory parameter missing'));
		return;
	}

	var socketId = hsData._query['socketId'];
	redisClient.get("eventProxyUser:"+socketId, function(err, value) {
		if (err) {
			next(new Error('not authorized'));
			return;
		}
		if (!value) {
			next(new Error('not authorized'));
			return;
		}
		value = PHPUnserialize.unserialize(value);
		socket.handshake.userId=value;
		hsData.userId = value;
		next();
	});
});
/*
 io.configure(function() {
 io.set('authorization', function(hsData, callback) {
 if (typeof(hsData.query['socketId']) == 'undefined') {
 callback("-1", false);
 return;
 }
 var socketId = hsData.query['socketId'];
 redisClient.get("eventProxyUser:"+socketId, function(err, value) {
 if (err) {
 callback("-2", false);
 return;
 }
 if (!value) {
 callback("-2", false);
 }
 value = PHPUnserialize.unserialize(value);
 hsData.userId = value;
 callback(null, true);
 });
 });
 });
 */
//----------------------------------------------------------------------------------------------------------------------------------------------
function broadcastAll(){
	var date = new Date();
	var time = date.getHours()+':'+date.getMinutes()+':'+date.getSeconds();

	for (var socketId in sockets) {
		sockets[socketId].emit('message', {'time': time, 'online': online, 'connected': sockets[socketId].handshake.userId});
	}
}

function updateUserOrders(userId, toOnline) {
	var queryResult = mySqlClient.query(preparedQueryToUpdateUsers, [(toOnline ? 1 : 0), parseInt(userId)]);
	sphinxUpdateCount++;

	queryResult.on('error', function(err) {
		console.log(err);
	});
}


setInterval(function() {
	redisClient.ping();
	mySqlClient.query('SELECT 1');
//    console.log("Redis ping performed");
}, 10000);

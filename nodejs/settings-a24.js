module.exports = {
    socket : {
        ioPort: 5680,
        backConnectPort : 19900
    },
    sphinx : {
        host: '192.168.1.7',
        port: 9311
    },
    redis : {
        host: '192.168.1.7',
        port: 6379,
        addedSettings: {'retry_max_delay': 5000}
    },
    node : {
        version: '0.11',
        environment: 'dev'
    }
};
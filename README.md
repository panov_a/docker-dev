# памятка

1. установить docker, docker-compose
2. git clone данной репы
3. настроить пути до проекта в docker-compose.yml (volumes в web и php)
4. прописать в /etc/hosts: "127.0.0.1   dev99.a24.local"
5. убедиться, что 80 порт свободен на локальной машине
6. костыль: в проекте в application/libraries/Base/Service/SiteBlock.php в методе isBlockedCity заменить $geoipData = geoip_record_by_name($ip) на $geoipData = false (почему то падает на этой функции, хотя расширение geoip установлено)
7. запустить docker-compose up --build

Проект будет доступен по адресу dev99.a24.local, база и редис dev'овские.

# todo

2. Пофиксить костыль из п.6 памятки
3. Почистить код dockerfile'ов и docker-compose
5. Вынести GeoIP базы из репы
7. На уровне офисной сети зарезолвить dev99 в localhost, если есть такая возможность

